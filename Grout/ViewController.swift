//
//  ViewController.swift
//  Grout
//
//  Created by Eric Betts on 10/1/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Cocoa
import CoreBluetooth

class ViewController: NSViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    let feec_uuid = CBUUID(string: "FEEC")
    
    var centralManager:CBCentralManager!
    var connectingPeripheral:CBPeripheral!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager){
        switch central.state{
        case .poweredOn:
            print("poweredOn")
            print("Scanning...")
            central.scanForPeripherals(withServices: [feec_uuid], options: nil)
        default:
            print(central.state)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("didDiscover", advertisementData)
        central.stopScan()
        connectingPeripheral = peripheral
        connectingPeripheral.delegate = self
        central.connect(peripheral, options: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnect \(peripheral.identifier)")
        peripheral.discoverServices(nil)
    }
    
    //-------
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard error == nil else {
            print("didDiscoverServices \(error)")
            return
        }
        
        for service in peripheral.services as [CBService]!{
            print("Service \(service.uuid)")
            if (service.uuid == CBUUID(string: "FEED")) {
                peripheral.discoverCharacteristics([], for: service)
            }
            
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard error == nil else {
            print("didDiscoverCharacteristicsFor \(error)")
            return
        }
        for characteristic in service.characteristics as [CBCharacteristic]! {
            print("Characteristic \(characteristic.uuid)")
            if characteristic.properties.contains(.notify) {
                print("\(characteristic.uuid) has notify")
                peripheral.setNotifyValue(true, for: characteristic)
            }
            if characteristic.properties.contains(.notifyEncryptionRequired) {
                print("\(characteristic.uuid) has notifyEncrytionRequired")
            }
            if characteristic.properties.contains(.read) {
                print("\(characteristic.uuid) has read: \(characteristic.value)")
                peripheral.readValue(for: characteristic)
            }
            if characteristic.properties.contains(.write) {
                print("\(characteristic.uuid) has write")
            }
            if characteristic.properties.contains(.writeWithoutResponse) {
                print("\(characteristic.uuid) has writeWithoutResponse")
            }
            if characteristic.properties.contains(.authenticatedSignedWrites) {
                print("\(characteristic.uuid) has authenticatedSignedWrites")
            }
            if characteristic.properties.contains(.indicate) {
                print("\(characteristic.uuid) has indicate")
            }
            if characteristic.properties.contains(.indicateEncryptionRequired) {
                print("\(characteristic.uuid) has indicateEncryptionRequired")
            }

        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            print("didUpdateValueFor characteristic \(error)")
            return
        }
        NSLog("C \(characteristic.uuid.uuidString): [\(characteristic.value!.count)] \(characteristic.value!)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            print("[ERROR] didUpdateNotificationStateFor \(characteristic.uuid): \(error)")
            return
        }
        print("didUpdateNotificationStateFor", characteristic.uuid)
    }
    
    
}

